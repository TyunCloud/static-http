# devops

#### 介绍
简单的前端静态页面运行容器


#### 简单使用
```shell
打包程序
shttp pack static-html-app/dist target/static-html-app.ssp

以当前目录启动http服务
shttp --port 8082

以指定目录启动http服务
shttp target/dist --port 8082
 
自动解包启动http服务
shttp -ssp target/static-html-app.ssp --port 8082
```
