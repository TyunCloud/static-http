
import { ChildCommand, MainCommander } from "./mainCommander";
import cluster from "cluster";
import compressing from "compressing";
import path from "path";
import fs from "fs";
import logger from "../common/logger";
import util from "../common/util";
import httpStaticService from "../service/HttpStaticService";
import httpOssService from "../service/HttpOssService";


const __cwd_path = process.cwd();

export class CommandMain extends ChildCommand {

    program!: MainCommander;



    public install(program: MainCommander): void {

        this.program = program;

        program
            .name("shttp")
            .argument("[rootDir]", "根目录路径")
            .option("-ssp,  --ssp   <sspFilePath>", "指定需要运行的打包文件(static server package)")
            .option("-oss,  --oss   <ossRootDir>", "指定存储根路径并开启OSS存储模式")
            .option("-p,    --port  <port>", "指定运行的端口(默认:80)")
            .option("-d,    --debug", "开启调试模式")
            .option("--worker", "工作进程标识,请勿使用该参数启动主进程")
            .option("--worker.count <workerCount>", "指定工作进程数，默认为1，设置为auto，则根据工作机器的cpu核心数")
            .option("--service.access <accessKey>", "指定访问访问秘钥 header.accesskey")
            .action(this.onMainCommandEvent.bind(this))

    }



    private async onMainCommandEvent(rootDir: string, option: any) {

        logger.isDebug = !!option.debug;

        if (cluster.isPrimary && option.worker) {
            logger.println("\n--worker 参数用于标识工作进程,请勿使用该参数启动主进程\n");
            return;
        }

        if (!!rootDir) {
            await this.onMainCommandEvent_root(rootDir, option);
        } else if (!!option.ssp) {
            await this.onMainCommandEvent_ssp(option.ssp, option);
        } else if (!!option.oss) {
            await this.onMainCommandEvent_oss(option.oss, option);
        } else {
            logger.println("请使用-h参数查看使用帮助");
        }

    }

    /**
     * 以传入的 root 路径启动静态资源服务
     * @param rootPath 根目录路径
     * @param option 
     */
    private async onMainCommandEvent_root(rootPath: string, option: any) {

        let option_workerCount = option["worker.count"] ?? 1;
        let option_port = option.port ?? 80;
        let root_dir = path.resolve(__cwd_path, rootPath ?? "./");

        if (!util.existDirSync(root_dir)) throw `不存在目录 ${root_dir} `;

        httpStaticService.workProcessCount = option_workerCount;
        httpStaticService.listen(root_dir, option_port);
    }

    /**
     * 以传入的ssp文件路径启动静态资源服务
     * @param sspPath ssp文件路径
     * @param option 
     */
    private async onMainCommandEvent_ssp(sspPath: string, option: any) {

        let option_workerCount = option["worker.count"] ?? 1;
        let option_port = option.port ?? 80;
        let ssp_file_abs = path.resolve(__cwd_path, sspPath);

        if (!util.existFileSync(ssp_file_abs)) throw `不存在文件 ${ssp_file_abs} `;

        let temp_dir = this.createTempDir(ssp_file_abs);
        await compressing.zip.uncompress(ssp_file_abs, temp_dir);
        httpStaticService.workProcessCount = option_workerCount;
        httpStaticService.listen(temp_dir, option_port);
    }



    /**
     * 以传入的rootPath启动OSS服务
     * @param rootPath  根目录路径
     * @param option 
     */
    private async onMainCommandEvent_oss(rootPath: string, option: any) {
        let option_workerCount = option["worker.count"] ?? 1;
        let option_accessKey = option["service.access"] ?? null;
        let option_port = option.port ?? 80;
        let root_dir = path.resolve(__cwd_path, rootPath ?? "./");

        util.initDirSync(rootPath);

        httpOssService.accessKey = option_accessKey;
        httpOssService.workProcessCount = option_workerCount;
        httpOssService.listen(root_dir, option_port);

    }


    /**
     * 创建临时目录
     * @param ssp_file 
     * @returns 
     */
    private createTempDir(ssp_file: string) {
        let ssp_dir_abs = path.resolve(ssp_file, "../");
        let ssp_filename = path.parse(ssp_file).base;
        let temp_dirname = `.temp.shttp.${util.MD5(ssp_filename)}`;

        let temp_dir_abs = path.resolve(ssp_dir_abs, temp_dirname);

        logger.debug(`mark temp dir:\t${temp_dir_abs}`);
        fs.rmSync(temp_dir_abs, { recursive: true, force: true });
        fs.mkdirSync(temp_dir_abs, { recursive: true });
        return temp_dir_abs;
    }

}



export default new CommandMain();
