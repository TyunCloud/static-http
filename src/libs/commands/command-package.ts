

import { ChildCommand, MainCommander } from "./mainCommander";
import compressing from "compressing";
import process from "process";
import path from "path";
import fs from "fs";
import logger from "../common/logger";
import util from "../common/util";


const __cwd_path = process.cwd();
export class CommandPackage extends ChildCommand {



    public install(program: MainCommander): void {

        program
            .command("pack")
            .argument("<sourceDirPath>", "待打包文件目录")
            .argument("[distFilePath]", "生成打包文件路径")
            .description("打包前端代码")
            .action(this.onCommandActionEvent.bind(this));
    }


    /**
     * 执行前端页面打包逻辑
     * @param src_dir 需打包的目录
     * @param dist_file 打包后保存的文件位置
     * @param options 
     */
    async onCommandActionEvent(src_dir: string, dist_file = "target/dist_package.ssp", options: any) {
        if (!src_dir) throw "需要指定参数 sourcePath ,详细情况请使用 -h 参数查看";
        if (!dist_file) throw "需要指定参数 distPath ,详细情况请使用 -h 参数查看";


        let src_dir_abs = path.resolve(__cwd_path, src_dir);
        let dist_dir_abs = path.resolve(__cwd_path, dist_file, "../");
        let dist_file_abs = path.resolve(__cwd_path, dist_file);

        if (!util.existDirSync(src_dir_abs)) throw `不存在目录 ${src_dir_abs} `;
        if (!util.existDirSync(dist_dir_abs)) fs.mkdirSync(dist_dir_abs, { recursive: true });

        logger.info(`packing ${src_dir_abs}`);
        await compressing.zip.compressDir(src_dir_abs, dist_file_abs, { ignoreBase: true });
        logger.info(`package ${dist_file_abs}`);
    }
}


export default new CommandPackage();