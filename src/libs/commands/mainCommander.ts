import { Command } from "commander";



export abstract class ChildCommand {
    /**
 * 通过重写该函数完成插件安装
 * @param program 主命令
 */
    public abstract install(program: MainCommander): void

}


export class MainCommander extends Command {

    use(child: ChildCommand) {
        child.install(this);
        return this;
    }
}

export default new MainCommander();