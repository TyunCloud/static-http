


/**
 * 解析字符串,将类似#[1,2m]的字符串提取出来，转换为平台特定的颜色标识
 * 示例：#[1,31m]这个文本是红色的#[0m]
 * 
 * linux	echo \033[1;31m222\033[0m
 * 
 * 1：高亮、4：下划线、5：闪烁
 * +------+-------+--------+
 * | 颜色 | 前景色 | 背景色 |
 * +------+-------+--------+
 * | 红色 |   31  |   41   |
 * | 绿色 |   32  |   42   |
 * | 黄色 |   33  |   43   |
 * | 蓝色 |   34  |   44   |
 * | 紫色 |   35  |   45   |
 * | 青色 |   36  |   46   |
 * | 白色 |   37  |   47   |
 * +------+-------+--------+
 * 
 * @param sourceText 
 * @returns 
 */
function _compileEchoText(sourceText: string) {

    let text = sourceText;
    let items = text.match(/#(\[[0-9;m]+)\]/g);
    if (!items) return text;
    for (let i = 0; i < items.length; i++) {
        let item = items[i];
        item = item.substr(0, item.length - 1);
        item = item.replace("#[", "\x1b[");
        text = text.replace(items[i], item);
    }

    return text

}



export default {

    isDebug: false,

    /**
     * 允许使用#[1,31m]方式定义文本输出颜色，该方法将根据特定平台将其进行转换
     * 
     * @param text 输出文本
     */
    print(text: string = "") {

        let echoText = _compileEchoText(text)
        process.stdout.write(echoText);
        return Promise.resolve(text);
    },




    /**
     * 允许使用#[1,31m]方式定义文本输出颜色，该方法将根据特定平台将其进行转换
     * 
     * @param text 输出文本
     */
    println(text: string = "") {
        return this.print(`${text}\n`);
    },


    /**
     * 允许使用#[1,31m]方式定义文本输出颜色，该方法将根据特定平台将其进行转换
     * 注！该方法将自动带有前缀[debug]，并以蓝色文本显示
     * 注！该方法是否输出值，受isDebug属性影响
     * @param text 输出文本
     * @param color 颜色值 31~37
     */
    debug(text: string = "", color = 34) {
        if (!this.isDebug) return Promise.resolve(text);

        if (text.startsWith("\t")) {
            return this.println(`#[${color}m]${text}#[0m]`);
        }
        else {
            return this.println(`#[${color}m][debug]${text}#[0m]`);
        }

    },



    /**
     * 允许使用#[1,31m]方式定义文本输出颜色，该方法将根据特定平台将其进行转换
     * 注！该方法将自动带有前缀[error]，并以红色文本显示
     * @param text 输出文本
     */
    error(text: string = "") {
        if (text.startsWith("\t")) {
            return this.println(`#[31m]${text}#[0m]`);
        } else {
            return this.println(`#[31m][error]${text}#[0m]`);
        }

    },

    /**
     * 允许使用#[1,31m]方式定义文本输出颜色，该方法将根据特定平台将其进行转换
     *  注！该方法将自动带有前缀[info]，并以绿色文本显示
     * @param text 输出文本
     */
    info(text: string = "") {
        if (text.startsWith("\t")) {
            return this.println(`#[32m]${text}#[0m]`);
        } else {
            return this.println(`#[32m][info]${text}#[0m]`);
        }
    },


    /**
     * 允许使用#[1,31m]方式定义文本输出颜色，该方法将根据特定平台将其进行转换
     *  注！该方法将自动带有前缀[warn]，并以黄色文本显示
     * @param text 输出文本
     */
    warn(text: string = "") {
        if (text.startsWith("\t")) {
            return this.println(`#[33m]${text}#[0m]`);
        } else {
            return this.println(`#[33m][warn]${text}#[0m]`);
        }

    },

}