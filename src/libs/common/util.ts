
import process from 'process';
import fs from 'fs';
import path from 'path';
import crypto from "crypto";
import logger from './logger';




const _platform = process.platform.toString();
const _isLinux = (_platform === "linux");
const _isWin32 = (_platform === "win32");



export default {

    /**
     * 检查目录并初始化
     * @param dirPath cwd的相对路径
     */
    initDirSync(dist: string) {

        if (this.existFileSync(dist)) throw `mkdir：${dist} is file`;
        if (this.existDirSync(dist)) return;
        fs.mkdirSync(dist, { recursive: true });
        
    },

    /**
     * 检查文件是否存在
     * @param filePath 绝对路径
     * @returns 
     */
    existFileSync(filePath: string) {

        if (!fs.existsSync(filePath)) return false;
        if (!fs.statSync(filePath).isFile()) return false;
        return true;
    },

    /**
     * 检查目录是否存在
     * @param dirPath 绝对路径
     * @returns 
     */
    existDirSync(dirPath: string) {

        if (!fs.existsSync(dirPath)) return false;
        if (!fs.statSync(dirPath).isDirectory()) return false;
        return true;
    },


    /**
     * 将绝对路径转换为相对路径
     * @param sour_abs_path 待转换的绝对路径
     * @returns 
     */
    transformToRelative(sour_abs_path: string) {
        if (!sour_abs_path) throw `util.transformToRelative [sour_abs_path] 参数必须有值`;

        let cwd = process.cwd();
        if (!sour_abs_path.startsWith(cwd)) throw `util.transformToRelative 路径${sour_abs_path},无法转换为相对于当前程序的路径`;


        let rel_path = sour_abs_path.replace(cwd, "");
        if (rel_path.startsWith("/")) {
            return rel_path.substring(1);
        } else if (rel_path.startsWith("\\")) {
            return rel_path.substring(1);
        } else {
            return rel_path;
        }
    },



    MD5(text: string) {
        return crypto.createHash('md5').update(text).digest("hex");
    },


    /**
     * 格式化时间日期
     * 
     * @param formatString 格式化字符串
     * @param dateTime 时间
     * @returns 
     */
    formatDate(formatString: string, dateTime: Date = new Date()) {
        if (!(dateTime instanceof Date)) return null;

        let fullYear = dateTime.getFullYear();
        let month = dateTime.getMonth() + 1;
        let date = dateTime.getDate();
        let hours = dateTime.getHours();
        let minutes = dateTime.getMinutes();
        let secouds = dateTime.getSeconds();
        let milli = dateTime.getMilliseconds();
        let milliString = Array.from({ length: 3 - ("" + milli).length }, () => { return "0" }).join("") + milli;


        let dateString = formatString;


        dateString = dateString.replace(/mi/ig, "" + milliString);
        dateString = dateString.replace(/yyyy/ig, "" + fullYear);
        dateString = dateString.replace(/yy/ig, fullYear < 10 ? "0" + fullYear : "" + fullYear);
        dateString = dateString.replace(/MM/g, month < 10 ? "0" + month : "" + month);
        dateString = dateString.replace(/M/g, "" + month);
        dateString = dateString.replace(/dd/ig, date < 10 ? "0" + date : "" + date);
        dateString = dateString.replace(/d/ig, "" + date);
        dateString = dateString.replace(/hh/ig, hours < 10 ? "0" + hours : "" + hours);
        dateString = dateString.replace(/h/ig, "" + hours);
        dateString = dateString.replace(/mm/g, minutes < 10 ? "0" + minutes : "" + minutes);
        dateString = dateString.replace(/m/g, "" + minutes);
        dateString = dateString.replace(/ss/ig, secouds < 10 ? "0" + secouds : "" + secouds);
        dateString = dateString.replace(/s/ig, "" + secouds);


        return dateString;

    },
}