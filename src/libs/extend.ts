

interface Array<T> {
    forEachAsync(callbackfn: (value: T, index?: number, array?: T[]) => Promise<void>, thisArg?: any): Promise<void>;
}


interface String {
    toInteger(): number;
}


Array.prototype.forEachAsync = async function (callbackfn: (value: any, index?: number, array?: any[]) => Promise<void>, thisArg?: any) {

    for (let i = 0; i < this.length; i++) {
        await callbackfn(this[i], i, this);
    }

}

String.prototype.toInteger = function (): number {

    let value = this.toString();
    try {
        return parseInt(value);
    } catch {
        throw `无效的端口号 ${value}`;
    }
}