
import path from "path";
import fs from "fs";
import fsPromises from "fs/promises";
import process, { exit } from "process";
import express, { Request, Response, NextFunction } from "express";
import multer from "multer";
import { HttpOssService } from './HttpOssService';
import logger from "../common/logger";
import util from "../common/util";





export default function (service: HttpOssService) {
    const _multer_dest = path.join(process.cwd(), ".temp");
    fs.rmSync(_multer_dest, { recursive: true, force: true });

    const _parser_multer = multer({ dest: _multer_dest }).single("objectData");

    const router = express.Router();
    router.get("/", _express_router_service_index.bind(service));
    router.get("/shttp", _express_router_service_shttp.bind(service));
    router.all("/shttp/*", _checker_router_access.bind(service));

    //配置解析form-data
    router.post("/shttp/*", _parser_multer);
    //上传文件
    router.post("/shttp/save", _checker_router_params_objectData.bind(service));
    router.post("/shttp/save", _checker_router_params_objectKey.bind(service));
    router.post("/shttp/save", _express_router_service_shttp_save.bind(service));
    //删除文件
    router.post("/shttp/dele", _checker_router_params_objectKey.bind(service));
    router.post("/shttp/dele", _express_router_service_shttp_dele.bind(service));
    return router;

}

function _express_router_service_index(this: HttpOssService, req: Request, res: Response) {

    res.send(`shttp object storage service`);
}



function _express_router_service_shttp(this: HttpOssService, req: Request, res: Response) {


    let { host } = req.headers;
    let host_url = `http://${host}`;

    let text = "";
    text += `shttp object storage service worker process PID:${process.pid}\n\n`;
    text += `SHTTP OSS API:\n`;
    text += `\t${host_url}/shttp\t\tGET  {explain:"查看工作进程信息"}\n`;
    text += `\t${host_url}/shttp/save\tPOST {explain:"OSS上传文件",header:[accessKey],form:[objectKey,objectData]}\n`;
    text += `\t${host_url}/shttp/dele\tPOST {explain:"OSS删除文件",header:[accessKey],form:[objectKey]}\n`;
    text += `SHTTP OSS PARAMS:\n`;
    text += `\taccessKey:\t用于鉴定上传权限的凭证\n`;
    text += `\tobjectKey:\t包含文件名的完整保存路径,该值不允许以'/','\\','.'开头\n`;
    text += `\tobjectData:\t上传的数据文件\n`;

    res.send(`<pre>${text}<pre>`);
}


/**
 * POST /shttp/save 保存文件
 */
function _express_router_service_shttp_save(this: HttpOssService, req: Request, res: Response) {


    let host_url = `http://${req.headers.host}`;
    let object_key = (<string>req.body.objectKey ?? "").trim();

    let object_path_old = req.file!.path;
    let object_path_new = path.join(this.serviceRoot, object_key);
    let object_path_new_dir = path.dirname(object_path_new);

    util.initDirSync(object_path_new_dir);
    fs.renameSync(object_path_old, object_path_new);

    let savePath = path.join("/", object_key).replace(/\\/g, "/");
    let url = `${host_url}${savePath}`;
    res.status(200).json({ code: 200, data: { path: savePath, url } });


}

/**
 * POST /shttp/dele 删除文件
 */
function _express_router_service_shttp_dele(this: HttpOssService, req: Request, res: Response) {

    let object_key = (<string>req.body.objectKey ?? "").trim();
    let object_path_abs = path.join(this.serviceRoot, object_key);
    fs.rmSync(object_path_abs, { force: true });
    res.status(200).json({ code: 200, message: "file deleted" });
}


/**
 * 验证 /shttp/* 访问权限控制
 */
function _checker_router_access(this: HttpOssService, req: Request, res: Response, next: NextFunction) {
    let { accesskey } = req.headers;
    if (this.accessKey && this.accessKey !== accesskey) {
        res.status(200).json({ code: 503, message: `accessKey 错误` });
    } else {
        next();
    }
}


/**
 * POST 效验参数objectKey
 */
function _checker_router_params_objectData(this: HttpOssService, req: Request, res: Response, next: NextFunction) {
    if (!req.file) {
        res.status(200).json({ code: 503, message: "not file" });
    } else {
        next();
    }
}
/**
 * POST 效验参数objectKey
 */
function _checker_router_params_objectKey(this: HttpOssService, req: Request, res: Response, next: NextFunction) {

    let object_key = (<string>req.body.objectKey ?? "").trim();

    if (!object_key) {
        let message = `缺少参数[objectKey]用于保存数据的包含文件名的完整路径`;
        res.status(200).json({ code: 503, message });
    } else if (!check_object_key(object_key)) {
        let message = `参数[key]不允许以'/','\\','.'开头`;
        res.status(200).json({ code: 503, message });
    } else {
        next();
    }


}



function check_object_key(key: string) {
    if (key.startsWith("/")) return false;
    if (key.startsWith("\\")) return false;
    if (key.startsWith("..")) return false;
    return true;
}