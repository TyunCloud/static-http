

import cluster, { Worker } from "cluster";
import process, { exit } from "process";
import express, { Request, Response, NextFunction } from "express";
import path from 'path';
import os, { NetworkInterfaceInfo } from "os";

import logger from "../common/logger";
import util from "../common/util";
import shttp_oss_router from "./HttpOssRouter";




export class HttpOssService {

    private _workProcessCount = 1;                  //工作进程数

    private _service_port!: number;                 //服务工作端口
    private _service_root!: string;                 //服务工作根目录
    private _service_accesskey: string | null = null;  //接口访问凭证



    public get workProcessCount() {
        return this._workProcessCount;
    }

    /**
     * 获取或设置工作线程数量，该数量需大于0，如果设置为 "auto" 则自动获取当前机器 cpu 核心数量
     */
    public set workProcessCount(value: number | "auto") {
        if (value === "auto") {
            this._workProcessCount = os.cpus().length;
        } else if (value < 1) {
            this._workProcessCount = 1;
        } else {
            this._workProcessCount = value;
        }
    }

    public get accessKey() { return this._service_accesskey; }

    /**
     * 获取或设置上传文件时使用的accessKey
     */
    public set accessKey(value: string | null) { this._service_accesskey = value; }


    public get serviceRoot() { return this._service_root; }



    /**
     * 开启监听服务
     * @param root 文件根目录
     * @param port 监听端口
     */
    public listen(root: string, port: number = 80) {
        this._service_port = port;
        this._service_root = root;


        if (cluster.isPrimary) {
            this.listen_master_startWorkerListen();
        } else {
            this.initWorkerEnvironment();
            this.listen_worker_startHttpService();
        }
    }

    /**
     * 在主进程中启动工作进程
     */
    private listen_master_startWorkerListen() {
        logger.info(`starting http oss service master PID:${process.pid}`);


        let rel_root_path = util.transformToRelative(this._service_root);
        if (!util.existDirSync(rel_root_path)) throw `指定的目录不存在：${rel_root_path}`;

        let args = ["-oss", rel_root_path, "--worker"];
        this.startWorkerProcess(args, this._workProcessCount);
    }

    /**
     * 在工作进程中启动http监听服务
     */
    private listen_worker_startHttpService() {
        logger.info(`starting object storage service worker PID:${process.pid}`);

        const _app = express();
        _app.use(shttp_oss_router(this));
        _app.use(express.static(this._service_root));
        _app.use(this.express_service_404.bind(this));
        _app.listen(this._service_port);
    }



    private express_service_404(req: Request, res: Response) {
        res.status(404).send("404 not found file");
    }


    /**
     * 在工作进程退出后，重新启动一条工作进程
     * @param worker 退出的工作进程
     */
    private onClusterWorkerEvent_Exit(worker: Worker, code: number, signal: string) {

        if (code === 0) return;
        logger.warn(`worker process exit PID:${worker.process.pid}`);
        let env = this.generateWorkerEnvironment();
        cluster.fork(env);

    }


    /**
     * 启动工作进程
     * @param args 启动参数
     * @param processCount 工作进程数
     */
    private startWorkerProcess(args: string[], processCount: number) {
        cluster.setupPrimary({ args });
        cluster.on("exit", this.onClusterWorkerEvent_Exit.bind(this));

        let env = this.generateWorkerEnvironment();
        for (let i = 0; i < processCount; i++) {
            cluster.fork(env);
        }
    }


    private initWorkerEnvironment() {
        this._service_port = parseInt(process.env["shttp.service.port"] ?? "-1");
        this._service_accesskey = process.env["shttp.service.access"] ?? null;
    }

    private generateWorkerEnvironment() {
        return {
            "shttp.service.port": this._service_port,
            "shttp.service.access": this._service_accesskey,
        };
    }

}

export default new HttpOssService();



