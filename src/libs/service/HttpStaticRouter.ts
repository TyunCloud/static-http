
import path from "path";
import fs, { readSync } from "fs";
import process, { exit } from "process";
import express, { Request, Response, NextFunction } from "express";
import { HttpStaticService } from './HttpStaticService';
import logger from "../common/logger";
import util from "../common/util";




export default function (service: HttpStaticService) {
 
    const router = express.Router();
    router.get("/", _express_router_service_index.bind(service));
    router.get("/shttp", _express_router_service_shttp.bind(service));
    
    return router;
}


function _express_router_service_index(this: HttpStaticService, req: Request, res: Response) {

    res.send(`shttp static resources service`);
}

function _express_router_service_shttp(this: HttpStaticService, req: Request, res: Response) {

    let text = "";
    text += `shttp static resources service worker process PID:${process.pid}`
    res.send(`<pre>${text}<pre>`);
}

