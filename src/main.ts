

import "./libs/extend";
import process from "process";
import logger from "./libs/common/logger";
import program from "./libs/commands/mainCommander";
import commandMain from "./libs/commands/command-main";
import commandHelp from "./libs/commands/command-help";
import commandPack from "./libs/commands/command-package";


/**
 * 打包程序
 * shttp pack static-html-app/dist target/static-html-app.ssp
 * 
 * 以当前目录启动http服务
 * shttp --port 8082
 * 
 * 以指定目录启动http服务
 * shttp target/dist --port 8082
 * 
 * 自动解包启动http服务
 * shttp --ssp target/static-html-app.ssp --port 8082
 * 
 * 启动OSS服务
 * shttp --oss=shttp_oss --service.access=123 --port=8023
 */

/*
    启动文件服务   ts-node .\static-http\src\main.ts shttp_oss --port=8023   
    启动OSS服务    ts-node .\static-http\src\main.ts --oss=shttp_oss --service.access=123 --port=8023
*/


//全局捕获未处理的Promise.catch
process.on("unhandledRejection", (reason: any, promise) => {
    logger.error(`Promise.catch:[${reason}]`);
});


(async function () {


    program.version("v1.3.2", "-v,    --version", "查看版本信息");
    program.use(commandHelp);
    program.use(commandMain);
    program.use(commandPack);
    //program.allowUnknownOption();
    //program.allowExcessArguments(true);
    //program.enablePositionalOptions();
    program.parseAsync(process.argv);

})();